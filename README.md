# Supercharger

Stop creating tags at start of your files, Let Supercharger take care of it. 

Supercharger will create tag with file name and calling method for you, 
So you can stop being paranoid and quit creating Tags in each of your file.

## Usage
Just type your message, Let supercharger take care of the tags.
```
Log.d("your message")
```

When you want to have delight of throwable with logs, Use
```
Log.d("your message", throwable)
```

Don'r worry if you are using Android's Log in your project.
If your Log statement looks something like this,
```
Log.d(TAG, your message")
```
We internally handle this and you don't need to do refactor.

Currently only DEBUG scope is supported, All other scopes will be added as soon as possible.

**Note: Please make sure you are using in.technowolf.supercharger.Log as your import package not android.util.Log**

## Setup

To use this library, First add Jitpack to your project

```
allprojects {
    repositories {
	    ...
	    maven { url 'https://jitpack.io' }
    }
}
```

Sync gradle files and add dependency,

Latest Version: [![](https://jitpack.io/v/daksh7011/supercharger.svg)](https://jitpack.io/#daksh7011/supercharger)

```
dependencies {
    implementation 'com.github.daksh7011:supercharger:$superchargerVersion'
}
```