# Security Policy

## Supported Versions

Version above 0.3 are supported and being continuously checked for any kind of security vulnerability.
Please note version below 0.3 are no longer being supported, Please update your versions.

| Version | Supported          |
| ------- | ------------------ |
| > 0.3   | :white_check_mark: |
|   v-0.2   | :x: |
|   v-0.1   | :x: |

## Reporting a Vulnerability

Please file an issue if you encounter or find any security vulnerability in this library.
