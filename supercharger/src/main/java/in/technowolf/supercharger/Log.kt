package `in`.technowolf.supercharger

import android.util.Log

class Log {

    companion object {

        //TODO: Documentation

        /**
         * generates the log with tag on debug level.
         *
         * @param message message you want to Log.
         * 
         * @return Unit
         * 
         * @since v-0.1
         * @author Daksh Desai
         */
        fun d(message: String) {
            Log.d(spawnTag(), message)
        }

        /**
         * appends extra string supplied with tag and generates the log with tag on debug level.
         * This method was introduced to provide flexible migration from android's Log.
         *
         * @param extra receives extra string to be append with tag
         * @param message message you want to Log
         * 
         * @return Unit
         * 
         * @since v-0.2
         * @author Daksh Desai
         */
        fun d(extra: String = "", message: String) {
            Log.d(spawnTag(extra), message)
        }

        /**
         * appends extra string supplied with tag and generates the log with tag on debug level.
         *
         * @param message message you want to Log.
         * @param throwable receives throwable supplied.
         * 
         * @return Unit
         * 
         * @since v-0.1
         * @author Daksh Desai
         */
        fun d(message: String, throwable: Throwable) {
            Log.d(spawnTag(), message, throwable)
        }

        /**
         * appends extra string supplied with tag and generates the log with tag and throwable on debug level.
         * This method was introduced to provide flexible migration from android's Log.
         *
         * @param extra receives extra string to be append with tag
         * @param message message you want to Log.
         * @param throwable receives throwable supplied.
         * 
         * @return Unit
         * 
         * @since v-0.2
         * @author Daksh Desai
         */
        fun d(extra: String = "", message: String, throwable: Throwable) {
            Log.d(spawnTag(extra), message, throwable)
        }

        /**
         * generates the log with tag on verbose level.
         *
         * @param message message you want to Log.
         *
         * @return Unit
         *
         * @since 0.3
         * @author Daksh Desai
         */
        fun v(message: String) {
            Log.v(spawnTag(), message)
        }

        /**
         * appends extra string supplied with tag and generates the log with tag on verbose level.
         * This method was introduced to provide flexible migration from android's Log.
         *
         * @param extra receives extra string to be append with tag
         * @param message message you want to Log
         *
         * @return Unit
         *
         * @since 0.3
         * @author Daksh Desai
         */
        fun v(extra: String = "", message: String) {
            Log.v(spawnTag(extra), message)
        }

        /**
         * appends extra string supplied with tag and generates the log with tag on verbose level.
         *
         * @param message message you want to Log.
         * @param throwable receives throwable supplied.
         *
         * @return Unit
         *
         * @since 0.3
         * @author Daksh Desai
         */
        fun v(message: String, throwable: Throwable) {
            Log.v(spawnTag(), message, throwable)
        }

        /**
         * appends extra string supplied with tag and generates the log with tag and throwable on debug level.
         * This method was introduced to provide flexible migration from android's Log.
         *
         * @param extra receives extra string to be append with tag
         * @param message message you want to Log.
         * @param throwable receives throwable supplied.
         *
         * @return Unit
         *
         * @since 0.3
         * @author Daksh Desai
         */
        fun v(extra: String = "", message: String, throwable: Throwable) {
            Log.v(spawnTag(extra), message, throwable)
        }

        // TODO: add more methods for rest of the scopes.


        /**
         * This method spawns tag with file name and caller method.
         *
         * @return tag with filename and calling method.
         * 
         * @author Daksh Desai
         * @since v-0.1
         */
        private fun spawnTag(): String {
            val tracerElem = Thread.currentThread().stackTrace[4]

            return spawnFileName(tracerElem.fileName) + "." + tracerElem.methodName
        }

        /**
         * This method spawns tag with @param extra, file name and caller method.
         *
         * @param extra receives extra to be append with tag
         * 
         * @return tag with filename and calling method.
         * 
         * @since v-0.2
         * @author Daksh Desai
         */
        private fun spawnTag(extra: String): String {
            val tracerElem = Thread.currentThread().stackTrace[4]
            var finalTag = spawnFileName(tracerElem.fileName) + "." + tracerElem.methodName
            if (!extra.isBlank()) finalTag += " $extra"
            return finalTag
        }

        /**
         * This method spawns file name from stack trace and removes extension of file.
         *
         * @param fileName file name received from stacktrace
         * 
         * @return file name with extension removed.
         * 
         * @since v-0.1
         * @author Daksh Desai
         */
        private fun spawnFileName(fileName: String?): String? {
            return if (fileName == null || fileName.isNullOrBlank() || !fileName.contains(".")) fileName
            else fileName.substring(0, fileName.lastIndexOf("."))
        }

    }
}
